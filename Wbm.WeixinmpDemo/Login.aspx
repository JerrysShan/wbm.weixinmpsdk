﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>获取用户认证地址 - 我不忙-微信公众平台演示</title>
</head>
<body>
    <div>
        <%
            /*
             * 3、获取用户认证地址。(参考Login.aspx文件)
             */
            var oauth = new Wbm.WeixinmpSDK.OAuths.Weixinmps.WeixinmpOAuth();
            if (oauth != null)
            {
                oauth.UpdateCache(DateTime.Now.AddHours(1)); //缓存当前协议
                var token = oauth.GetAccessToken();
                if (token.ret == 0)
                {
                    oauth.AccessToken = token.access_token;
                    oauth.ExpiresIn = token.expires_in;
                    oauth.UpdateCache(); //缓存认证信息

                    Response.Redirect("./");
                }
                else
                {
                    Response.Write(token.msg + "(" + token.errcode + ")");
                    Response.Write("<br />" + token.response);
                }
            }
            else
            {
                Response.Write("登录失败，找不到相对应的接口");
            }
           
        %>
    </div>
</body>
</html>
